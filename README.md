## Aurelia Demo

Small application to demo aurelia framework differences and features.

#### To Install
1. `cd` to `aurelia-demo` directory
2. `npm install`
3. `jspm install`
4. `http-server -o -c-1 -p 4200`