import {inject} from 'aurelia-framework';

import {DemoService} from './demo.service';
import './demo.css!css';

@inject(DemoService)
export class Demo {
	constructor(demoService) {
		this.demoService = demoService;

		this.message = "Component: GO!";
	}

	callService() {
		this.fromService = this.demoService.getText();
	}
}