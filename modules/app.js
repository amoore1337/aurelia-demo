import './global.css!css';

export class App {
	constructor() {
		this.heading = "Hello World!";
	}

	configureRouter(config, router) {
		this.router = router;
		config.title = "Root";
		config.options.root = "/";
		// config.options.pushState = true;
		config.map([
			{route: '', redirect: 'demo' },
			{route: 'demo', name:"demo", moduleId:'./demo/demo', title:'Demo', nav: true},
			{route: 'new-state', name:"newState", moduleId:'./new-state/new-state', title:'New State', nav: true}
		]);
	}
}